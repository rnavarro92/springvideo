package navarro.ricardo.dao;

import navarro.ricardo.beans.Marca;

public interface DAOMarca {
	
	public void registrar(Marca marca)throws Exception;

}
