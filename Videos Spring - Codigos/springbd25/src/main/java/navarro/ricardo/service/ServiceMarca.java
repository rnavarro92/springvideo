package navarro.ricardo.service;

import navarro.ricardo.beans.Marca;

public interface ServiceMarca {
	
	public void registrar(Marca marca) throws Exception;

}
