package navarro.ricardo.springbd25;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import navarro.ricardo.beans.Marca;
import navarro.ricardo.service.ServiceMarca;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Marca marca = new Marca();
        marca.setId(2);
        marca.setNombre("Marca2");
        
        ApplicationContext appContext = new ClassPathXmlApplicationContext("navarro/ricardo/xml/beans.xml");
        ServiceMarca sm = (ServiceMarca)appContext.getBean("serviceMarcaImpl");
        Marca mar3 = (Marca)appContext.getBean("marca3");
        
        try {
			sm.registrar(mar3);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
    }
}
