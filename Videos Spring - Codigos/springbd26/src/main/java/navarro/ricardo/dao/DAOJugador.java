package navarro.ricardo.dao;

import navarro.ricardo.beans.Jugador;

public interface DAOJugador {
	
	public void registrar(Jugador jugador) throws Exception;

}
