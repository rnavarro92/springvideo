package navarro.ricardo.springbd25;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import navarro.ricardo.beans.Equipo;
import navarro.ricardo.beans.Jugador;
import navarro.ricardo.beans.Marca;
import navarro.ricardo.service.ServiceJugador;
import navarro.ricardo.service.ServiceMarca;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        
        
        ApplicationContext appContext = new ClassPathXmlApplicationContext("navarro/ricardo/xml/beans.xml");
        ServiceJugador sj = (ServiceJugador)appContext.getBean("serviceJugadorImpl");
        Jugador jugador = (Jugador)appContext.getBean("jugador1");
        
        try {
			 sj.registrar(jugador);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
    }
}
