package navarro.ricardo.springvideo1;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import navarro.ricardo.beans.Barcelona;
import navarro.ricardo.beans.Ciudad;
import navarro.ricardo.beans.Jugador;
import navarro.ricardo.beans.Juventus;
import navarro.ricardo.beans.Persona;
import navarro.ricardo.interfaces.IEquipo;


public class App {
	
	public static void main (String [] args){
		//ApplicationContext appContext = new ClassPathXmlApplicationContext("navarro/ricardo/xml/beans.xml");
		
		ApplicationContext appContext = new ClassPathXmlApplicationContext("navarro/ricardo/xml/beans.xml");
//		Jugador ju = (Jugador) appContext.getBean("messi");
//		
//		System.out.println(ju.getNombre()+ " - " +ju.getEquipo().mostrar() );
		
		Jugador jug = (Jugador) appContext.getBean("messi");
//		Juventus juv = (Juventus) appContext.getBean("juventus");
		System.out.println(jug.getNombre()+ " - " + jug.getEquipo().mostrar());
		
		((ConfigurableApplicationContext)appContext).close();
	}

}
