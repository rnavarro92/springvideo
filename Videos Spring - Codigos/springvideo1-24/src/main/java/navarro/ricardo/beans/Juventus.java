package navarro.ricardo.beans;

import org.springframework.stereotype.Component;

import navarro.ricardo.interfaces.IEquipo;

@Component
public class Juventus implements IEquipo{

	public String mostrar() {
		
		return "Juventus";
	}

}
