package navarro.ricardo.springvideo1;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import navarro.ricardo.beans.Barcelona;
import navarro.ricardo.beans.Camiseta;
import navarro.ricardo.beans.Jugador;
import navarro.ricardo.beans.Juventus;
import navarro.ricardo.beans.Marca;

@Configuration
public class AppConfig {
	
	@Bean
	public Jugador jugador1(){
		return new Jugador();
	}
	@Bean
	public Barcelona barcelona(){
		return new Barcelona();
	}
	
	@Bean
	public Barcelona barcelona1(){
		return new Barcelona();
	}
	
	@Bean
	public Juventus juventus(){
		return new Juventus();
	}
	@Bean
	public Camiseta camiseta(){
		return new Camiseta();
	}
	@Bean
	public Marca marca(){
		return new Marca();
	}

}
