package navarro.ricardo.beans;

public class Ciudad {
	
	private String nombre;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	//no importa el nombre del metodo init o destroy solo hay que tener cuidado al seleccionarlos en el xml.
//	private void init(){
//		System.out.println("Antes de inicializar el bean Ciudad");
//	}
//	
//	private void destroy(){
//		System.out.println("Bean Ciudad a punto de ser destruido");
//	}
	

}
