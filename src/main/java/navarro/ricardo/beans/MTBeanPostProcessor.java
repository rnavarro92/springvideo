package navarro.ricardo.beans;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class MTBeanPostProcessor implements BeanPostProcessor{

	public Object postProcessAfterInitialization(Object bean, String nombreBean) throws BeansException {
		// TODO Auto-generated method stub
		System.out.println("Despues de la inicializacion " + nombreBean);
		return bean;
	}

	public Object postProcessBeforeInitialization(Object bean, String nombreBean) throws BeansException {
		// TODO Auto-generated method stub
		System.out.println("Antes de la inicializacion " + nombreBean);
		return bean;
	}

}
