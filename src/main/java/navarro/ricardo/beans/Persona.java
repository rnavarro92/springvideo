package navarro.ricardo.beans;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class Persona implements InitializingBean, DisposableBean{

	private int id;
	private String nombre;
	private String apodo;
	private Pais pais;
	private Ciudad ciudad;
	
	
	//no importa el nombre del metodo init o destroy solo hay que tener cuidado al seleccionarlos en el xml.
//	@PostConstruct
//	private void init(){
//		System.out.println("Antes de inicializar el bean Persona");
//	}
//	
//	@PreDestroy
//	private void destroy(){
//		System.out.println("Bean Persona a punto de ser destruido");
//	}
	
	
	public Ciudad getCiudad() {
		return ciudad;
	}

	public void setCiudad(Ciudad ciudad) {
		this.ciudad = ciudad;
	}

	public Pais getPais() {
		return pais;
	}

	public void setPais(Pais pais) {
		this.pais = pais;
	}
//
//	public Persona (int id, String nombre, String apodo){
//		this.id = id;
//		this.nombre = nombre;
//		this.apodo = apodo;
//	}
//	
//	public Persona (int id){
//		this.id = id;
//	}
	
//	public Persona (String apodo){
//		this.apodo = apodo;
//	}
	
	public void setId(int id) {
		this.id = id;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setApodo(String apodo) {
		this.apodo = apodo;
	}

	public int getId() {
		return id;
	}

	public String getNombre() {
		return nombre;
	}

	public String getApodo() {
		return apodo;
	}

	public void afterPropertiesSet() throws Exception {
		System.out.println("After persona");
		
	}

	public void destroy() throws Exception {
		System.out.println("Destroy persona");
		
	}

}
