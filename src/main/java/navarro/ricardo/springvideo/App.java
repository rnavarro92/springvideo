package navarro.ricardo.springvideo;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import navarro.ricardo.beans.AppConfig2;
import navarro.ricardo.beans.AppConfing;
import navarro.ricardo.beans.Barcelona;
import navarro.ricardo.beans.Ciudad;
import navarro.ricardo.beans.Jugador;
import navarro.ricardo.beans.Juventus;
import navarro.ricardo.beans.Mundo;
import navarro.ricardo.beans.Persona;
import navarro.ricardo.interfaces.IEquipo;



public class App {

	public static void main(String[] args) {
		
		ApplicationContext appContext = new ClassPathXmlApplicationContext("navarro/ricardo/xml/beans.xml");
		Jugador jug = (Jugador) appContext.getBean("messi");
		
//		Barcelona bar = (Barcelona) appContext.getBean("barcelona");
//		Juventus juv = (Juventus) appContext.getBean("juventus");
//		IEquipo ie = (IEquipo) appContext.getBean("juventus");

		
//		System.out.println(jug.getNombre() + " - " + jug.getEquipo().mostrar());
		
		System.out.println(jug.getNombre() + " - " + jug.getEquipo().mostrar());
		
		
		((ConfigurableApplicationContext)appContext).close(); 
		
		
	}

}
